package core.shape;

import core.*;

/**
 * Created by thomas on 6/3/15.
 */
public interface Shape {
    void draw(Artist artist);
}
