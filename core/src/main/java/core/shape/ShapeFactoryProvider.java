package core.shape;

/**
 * Created by thomas on 6/4/15.
 */
public interface ShapeFactoryProvider {
    Iterable<ShapeFactory> getShapeFactories();
}
