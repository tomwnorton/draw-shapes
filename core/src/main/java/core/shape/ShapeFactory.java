package core.shape;

import java.util.*;

/**
 * Created by thomas on 6/3/15.
 */
public interface ShapeFactory {
    public Shape create(Map<String, Double> parameters);
    public String getName();
    public List<String> getParameters();
}
