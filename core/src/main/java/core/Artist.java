package core;

/**
 * Created by thomas on 6/4/15.
 */
public interface Artist {
    void drawSolidRectangle(double left, double top, double width, double height);
}
