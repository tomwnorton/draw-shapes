import core.shape.*;
import javafx.application.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

import java.util.*;
import java.util.stream.*;

public class Main extends Application {
    private GraphicsContext gc;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello World!");

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(createMenuBar(primaryStage));

        Canvas canvas = new Canvas(250, 200);
        gc = canvas.getGraphicsContext2D();
        borderPane.setCenter(canvas);

        primaryStage.setScene(new Scene(borderPane, 300, 250));
        primaryStage.show();
    }

    private MenuBar createMenuBar(Stage primaryStage) {
        MenuItem fileExit = new MenuItem("Exit");
        fileExit.setOnAction(e -> primaryStage.close());

        Menu fileMenu = new Menu("File");
        fileMenu.getItems().add(fileExit);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(fileMenu);

        Menu shapesMenu = new Menu("Shapes");
        for (ShapeFactory shapeFactory : new ServiceLoaderShapeFactoryProvider().getShapeFactories()) {
            MenuItem item = new MenuItem(shapeFactory.getName());
            item.setOnAction(e -> drawShape(shapeFactory));
            shapesMenu.getItems().add(item);
        }
        if (!shapesMenu.getItems().isEmpty()) {
            menuBar.getMenus().add(shapesMenu);
        }
        return menuBar;
    }

    private void drawShape(ShapeFactory shapeFactory) {
        Dialog<Map<String, Double>> dialog = new Dialog<>();
        dialog.setTitle("Create " + shapeFactory.getName());
        dialog.setHeaderText("Fill in the parameters");

        ButtonType createButtonType = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(createButtonType, ButtonType.CANCEL);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(20, 150, 10, 10));

        Map<String, TextField> textFields = new HashMap<>();
        for (String propertyName : shapeFactory.getParameters()) {
            Label label = new Label(propertyName);
            TextField textField = new TextField("0.0");
            textFields.put(propertyName, textField);
            gridPane.addRow(textFields.size(), label, textField);
        }
        dialog.getDialogPane().setContent(gridPane);
        dialog.setResultConverter(
                dialogButton ->
                        textFields.entrySet()
                            .stream()
                            .collect(Collectors.toMap(entry -> entry.getKey(),
                                                      entry -> Double.valueOf(entry.getValue().getText()))));

        Optional<Map<String, Double>> result = dialog.showAndWait();
        result.ifPresent(map -> shapeFactory.create(map).draw(new JavaFxArtist(gc)));
    }
}
