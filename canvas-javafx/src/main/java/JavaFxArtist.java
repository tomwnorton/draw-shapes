import core.*;
import javafx.scene.canvas.*;

public class JavaFxArtist implements Artist {
    private final GraphicsContext graphicsContext;

    public JavaFxArtist(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    @Override
    public void drawSolidRectangle(double left, double top, double width, double height) {
        graphicsContext.fillRect(left, top, width, height);
    }
}
