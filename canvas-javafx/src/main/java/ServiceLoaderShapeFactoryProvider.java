import core.shape.*;

import java.util.*;

public class ServiceLoaderShapeFactoryProvider implements ShapeFactoryProvider {
    public Iterable<ShapeFactory> getShapeFactories() {
        return ServiceLoader.load(ShapeFactory.class);
    }
}
