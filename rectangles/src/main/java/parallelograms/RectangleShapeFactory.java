package parallelograms;

import core.shape.*;

import java.util.*;
import java.util.stream.*;

public class RectangleShapeFactory implements ShapeFactory {
    private enum Parameters {
        LEFT,
        TOP,
        WIDTH,
        HEIGHT;

        public String getKey() {
            return name().toLowerCase();
        }
    }

    @Override
    public Shape create(Map<String, Double> parameters) {
        guardAgainstBadParameters(parameters);

        double left = parameters.get(Parameters.LEFT.getKey());
        double top = parameters.get(Parameters.TOP.getKey());
        double width = parameters.get(Parameters.WIDTH.getKey());
        double height = parameters.get(Parameters.HEIGHT.getKey());

        return new Rectangle(left, top, width, height);
    }

    private void guardAgainstBadParameters(Map<String, Double> parameters) {
        if (parameters.get(Parameters.LEFT.getKey()) == null) {
            throw new IllegalArgumentException("Expected floating-point argument left");
        }
        if (parameters.get(Parameters.TOP.getKey()) == null) {
            throw new IllegalArgumentException("Expected floating-point argument top");
        }
        if (parameters.get(Parameters.WIDTH.getKey()) == null) {
            throw new IllegalArgumentException("Expected floating-point argument width");
        }
        if (parameters.get(Parameters.HEIGHT.getKey()) == null) {
            throw new IllegalArgumentException("Expected floating-point argument height");
        }
    }

    @Override
    public String getName() {
        return "Rectangle";
    }

    @Override
    public List<String> getParameters() {
        return Arrays.stream(Parameters.values()).map(parameter -> parameter.getKey()).collect(Collectors.toList());
    }
}
