package parallelograms;

import core.shape.*;

import java.util.*;
import java.util.stream.*;

public class SquareFactory implements ShapeFactory {
    private enum Parameter {
        LEFT,
        TOP,
        SIZE;

        public String getKey() {
            return name().toLowerCase();
        }
    }

    @Override
    public Shape create(Map<String, Double> parameters) {
        guardAgainstBadParameters(parameters);

        double left = parameters.get(Parameter.LEFT.getKey());
        double top = parameters.get(Parameter.TOP.getKey());
        double size = parameters.get(Parameter.SIZE.getKey());

        return new Square(left, top, size);
    }

    private void guardAgainstBadParameters(Map<String, Double> parameters) {
        if (parameters.get(Parameter.LEFT.getKey()) == null) {
            throw new IllegalArgumentException("Expected floating-point argument left");
        }
        if (parameters.get(Parameter.TOP.getKey()) == null) {
            throw new IllegalArgumentException("Expected floating-point argument top");
        }
        if (parameters.get(Parameter.SIZE.getKey()) == null) {
            throw new IllegalArgumentException("Expected floating-point argument size");
        }
    }

    @Override
    public String getName() {
        return "Square";
    }

    @Override
    public List<String> getParameters() {
        return Arrays.stream(Parameter.values()).map(parameter -> parameter.getKey()).collect(Collectors.toList());
    }
}
