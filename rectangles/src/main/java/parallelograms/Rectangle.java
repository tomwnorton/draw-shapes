package parallelograms;

import core.*;
import core.shape.*;

public class Rectangle implements Shape {
    private double left;
    private double top;
    private double width;
    private double height;

    public Rectangle(double left, double top, double width, double height) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw(Artist artist) {
        artist.drawSolidRectangle(left, top, width, height);
    }
}
