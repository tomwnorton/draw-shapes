package parallelograms;

import core.*;
import core.shape.*;

public class Square implements Shape {
    private double left;
    private double top;
    private double size;

    public Square(double left, double top, double size) {
        this.left = left;
        this.top = top;
        this.size = size;
    }

    @Override
    public void draw(Artist artist) {
        artist.drawSolidRectangle(left, top, size, size);
    }
}
