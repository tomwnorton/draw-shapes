package parallelograms;

import core.*;
import org.junit.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RectangleTests {
    @Test
    public void it_draws_the_correct_rectangle() {
        final double expectedLeft = 1.0;
        final double expectedTop = 5.0;
        final double expectedWidth = 10.0;
        final double expectedHeight = 20.0;
        Artist artist = mock(Artist.class);
        Rectangle rectangle = new Rectangle(expectedLeft, expectedTop, expectedWidth, expectedHeight);

        rectangle.draw(artist);

        verify(artist).drawSolidRectangle(expectedLeft, expectedTop, expectedWidth, expectedHeight);
    }
}
