package parallelograms;

import core.*;
import org.junit.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SquareTests {
    @Test
    public void it_draws_the_correct_square() {
        final double expectedLeft = 1.0;
        final double expectedTop = 5.0;
        final double expectedSize = 10.0;
        Artist artist = mock(Artist.class);
        Square rectangle = new Square(expectedLeft, expectedTop, expectedSize);

        rectangle.draw(artist);

        verify(artist).drawSolidRectangle(expectedLeft, expectedTop, expectedSize, expectedSize);
    }
}
